
# PHP Parse Meta Tags Script

```php

$tags = get_meta_tags('https://www.example.com');

echo $tags['author'].'<br>';  
echo $tags['keywords'].'<br>';   
echo $tags['description'].'<br>'; 
echo $tags['geo_position'].'<br>';
echo $tags['robots'].'<br>';
echo $tags['copyright'].'<br>';
echo $tags['googlebot'].'<br>';
echo $tags['language'].'<br>';
echo $tags['reply-to'].'<br>';
echo $tags['web_author'].'<br>';
echo $tags['ROBOTS'].'<br>'; 
```